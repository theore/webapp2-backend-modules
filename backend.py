import webapp2


class BackendHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write("hello world")


app = webapp2.WSGIApplication([
    ('/', BackendHandler),
], debug=True)
